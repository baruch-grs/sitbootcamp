# Webcomponents 

### Se basan en: 
   * Custom Elements
   * ShadowDom
   * Los imports
   * HTML Template

## Polymer.

    Es un _framework_ de Google basado en **webcomponents** y en librerías que ellos mismos desarrollan para hacer más fácil a nosotros el desarrollo de aplicaciones web.

Creaciíon de un componente

 - $mkdir header && cd header

 - $polymer init

Al ejecutar estos comandos nos creará diversos archivos y carpetas, entre ellas se encuentran:

  - demo: Contiene los archivos necesarios para realizar ejemplos de la implementación de nuestro componente
  
  - test: Contiene los archivos para la ejecución de pruebas unitarias y así probar la funcionalidad de nuestro componente.

  - .gitignore: archivo de configuración de git el cual contiene los archivos y carpetas a ignorar al momento de revisar los cambios en el código dentro del componente
  
  - bower.json: archivo que nos permite manejar las dependencias de nuestro componente

  - header-element.html: archivo que contiene todo el código de nuestro componente (tener en cuenta que el nombre puede cambiar entonces una sintaxis de notación más real sería algo así:
        * [nombre de mi elemento]-element.html)

##Definición de un componente POLYMER:

  - <template>. Dentro de la etiqueta template es donde se define el shado-dom del componente

  - **La clase** Se define la clase en JavaScript de el nuevo componente y _hereda_ todas las propiedades y métodos de **Polymer.Element**

  - **El registro** de su clase en el navegador. (window.customElements.define(HeaderElement.is, HeaderElement)).

